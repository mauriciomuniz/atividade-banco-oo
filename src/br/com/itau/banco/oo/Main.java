package br.com.itau.banco.oo;

public class Main {

    public static void main(String[] args) {

        Conta contaJoao = new Conta();
        Cliente clienteJoao = new Cliente();

        // cliente 01

        contaJoao.setNumeroConta(0001);
        contaJoao.setSaldo(1000.0);

        clienteJoao.setNome("Joao Julio");
        clienteJoao.setCpf("123321");
        clienteJoao.setIdade(17);

        System.out.println(clienteJoao.verificaIdade(clienteJoao.getIdade()));
    }
}
