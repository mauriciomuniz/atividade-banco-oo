package br.com.itau.banco.oo;

public class Conta {

    private Integer numeroConta;
    private Double saldo;
    private String saldoInsuficiente = "Saldo Insuficiente!";



    public Object saca(double valorSaque){
        if (valorSaque > saldo) {
            saldo-=valorSaque;
            return saldo;
        } else {
            return saldoInsuficiente;
        }

    }

    public double deposita(double valorDeposito){
        saldo+=valorDeposito;
        return saldo;
    }

    public Integer getNumeroConta() {
        return numeroConta;
    }

    public void setNumeroConta(Integer numeroConta) {
        this.numeroConta = numeroConta;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }
}
