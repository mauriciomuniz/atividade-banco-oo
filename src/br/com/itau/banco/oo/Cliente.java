package br.com.itau.banco.oo;

public class Cliente {

    private String nome;
    private Integer idade;
    private String cpf;
    private String idadeInsuficiente = "Idade Insuficiente";
    private String idadeOk = "Idade Ok";
    //public Cliente(String nome, Integer idade, String cpf) {
    //   this.cpf = cpf;
    //    this.nome = nome;
    //   this.idade = idade;
    //}

    public String verificaIdade(Integer idade) {
        if (idade >= 18) {
            return idadeOk;
        } else
            return idadeInsuficiente;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getIdade() {
        return idade;
    }

    public void setIdade(Integer idade) {
        this.idade = idade;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
}
